## Common config on all nodes

### Disable SELinux
    sudo /sbin/setenforce 0
    sudo sed -i s/SELINUX=enforcing/SELINUX=disabled/g /etc/sysconfig/selinux /etc/selinux/config

### Update system
    sudo yum -y update

### Add EPEL 7
    sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

### Add IUS
    sudo rpm -Uvh https://centos7.iuscommunity.org/ius-release.rpm

### Add Percona
    sudo rpm -Uvh http://www.percona.com/downloads/percona-release/redhat/0.1-4/percona-release-0.1-4.noarch.rpm

### Remove mysql libs - this breaks postfix
    sudo yum -y remove mysql-libs

### Update system - again
    sudo yum -y update

### Install Utilities
    sudo yum -y install bash-completion bzip2 expect fail2ban ftp htop iptraf-ng lftp lsof mailx mc nano nc ncftp nmap ntpdate p7zip pwgen rdate rsync sudo sharutils sysstat tcping tmux unzip vim vnstat wget yum-cron yum-plugin-replace yum-versionlock zip

### Start SysStat
    sudo systemctl enable sysstat
    sudo systemctl start sysstat


### Start VnStat
    ip link show | egrep '[0-9]:.[a-z]+[0-9]+' | awk -F: '{print "sudo vnstat --create -i "$2}' | sudo bash -x
    ip link show | egrep '[0-9]:.[a-z]+[0-9]+' | awk -F: '{print "sudo vnstat --enable -i "$2}' | sudo bash -x
    sudo chown -R vnstat: /var/lib/vnstat
    sudo systemctl enable vnstat
    sudo systemctl start vnstat

### Kernel tuning
    echo 'vm.swappiness = 10' | sudo tee /etc/sysctl.d/97-swappiness.conf
    sudo sysctl --system
    
### Use clusterssh for export the hostname
    export NEWNAME=## Go to > Send > Remote hostname
    echo $NEWNAME
    sudo hostnamectl set-hostname $NEWNAME

### Adjust your time/date settings
    sudo timedatectl set-timezone Continent/City

### Add your custom network settings

### Clean history
rm -rf .bash_history ; history -c 

### Reboot
sudo reboot; logout



# APP SERVERS ONLY

### Yum variables
    # Export php variable manually can be
    # php56u php70u or php71u
    export IUSPHP=php56u
    # Export percona variable manually can be
    # 55 56 or 57
    export PERCONA=57

### Install app packages

    sudo yum -y install httpd httpd-manual httpd-tools mod_fcgid mod_ldap mod_proxy_html mod_session mod_ssl mod_xsendfile webalizer $IUSPHP $IUSPHP-bcmath $IUSPHP-cli $IUSPHP-common $IUSPHP-dba $IUSPHP-debuginfo $IUSPHP-embedded $IUSPHP-enchant $IUSPHP-fpm $IUSPHP-gd $IUSPHP-gmp $IUSPHP-imap $IUSPHP-interbase $IUSPHP-intl $IUSPHP-ldap $IUSPHP-mbstring $IUSPHP-mcrypt $IUSPHP-mysqlnd $IUSPHP-odbc $IUSPHP-opcache $IUSPHP-pdo $IUSPHP-pecl-imagick $IUSPHP-pecl-memcached $IUSPHP-pecl-redis $IUSPHP-pecl-xdebug $IUSPHP-pgsql $IUSPHP-process $IUSPHP-recode $IUSPHP-snmp $IUSPHP-soap $IUSPHP-tidy $IUSPHP-xml $IUSPHP-xmlrpc postfix Percona-Server-shared-$PERCONA

### Enable firewall rules
    sudo firewall-cmd --zone=public --add-service=http
    sudo firewall-cmd --zone=public --add-service=https
    sudo firewall-cmd --zone=public --add-service=http --permanent
    sudo firewall-cmd --zone=public --add-service=https --permanent
    sudo firewall-cmd --zone=public --add-port=8080/tcp
    sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
    
    
### GlusterFS - Only App Servers

### Install packages
    sudo yum -y install centos-release-gluster
    sudo yum -y install glusterfs-server
    sudo systemctl enable glusterd
    sudo systemctl start glusterd

### Export path for app server volume
    export HTDOCSDEV=   # Block device here

### Export path for logs serve volume
    export HTLOGSDEV=   # Block device here

### Create volumes
    sudo mkfs.xfs -L HTDOCS $HTDOCSDEV
    sudo mkfs.xfs -L HTLOGS $HTLOGSDEV

### Set the brick (If uses GlusterFS)
    sudo mkdir -p /glusterfs/htdocs
    grep HTDOCS /etc/fstab || echo "LABEL=HTDOCS /glusterfs/htdocs xfs defaults 0 0" | sudo tee -a /etc/fstab


### Set the log volume
    grep HTLOGS /etc/fstab || echo "LABEL=HTLOGS /var/log/httpd xfs defaults 0 0" | sudo tee -a /etc/fstab
    sudo mount -a

### Start the service
    sudo systemctl enable glusterd.service
    sudo systemctl start glusterd.service

### Peer probe (Node1 - Manually)
    sudo gluster peer probe # add each hostname and then
    sudo gluster peer status

### Create the volume (Node1 Only) - Change the example nodenames
    sudo gluster volume create htdocs replica 2 transport tcp gluster1.example.com:/glusterfs/htdocs/data gluster2.example.com:/glusterfs/htdocs/data

### GlusterFS tuning
    sudo gluster volume set htdocs cluster.min-free-disk 10%
    sudo gluster volume set htdocs performance.cache-size 2GB
    sudo gluster volume set htdocs performance.cache-max-file-size 64MB
    sudo gluster volume set htdocs performance.cache-refresh-timeout 10

### Start the volumes (Node1 Only)
    sudo gluster volume start htdocs

### Copy data from /var/www & /var/log (Node1 Only)
    sudo mount -t glusterfs localhost:/htdocs /mnt/
    sudo rsync -auvg /var/www/* /mnt/.
    sudo umount /mnt

### Ot Set the log volume (If is a standard filesystem)
    grep HTDOCS /etc/fstab || echo "LABEL=HTDOCS /var/www xfs defaults 0 0" | sudo tee -a /etc/fstab
    sudo mount LABEL=HTDOCS /mnt/
    sudo rsync -auvg /var/www/* /mnt/.
    sudo umount /mnt
    sudo mount -a

### Clean the http directories all hosts
    sudo rm -rfv /var/www/*

### Mount all Gluster fs volumes
    grep '/var/www' /etc/fstab || echo "localhost:/htdocs /var/www glusterfs defaults,_netdev 0 0" | sudo tee -a /etc/fstab
    sudo mount -a

### Verify GlusterFS Volumes:
    sudo gluster volume info all

### Create NetworkManager dispatcher for the "glustered" interface
    sudo touch /etc/NetworkManager/dispatcher.d/99-httpd-glusterfs
    sudo chmod 755 /etc/NetworkManager/dispatcher.d/99-httpd-glusterfs
    sudo vi /etc/NetworkManager/dispatcher.d/99-httpd-glusterfs

### File contents
    #!/bin/sh
    # This is a dispatcher script for mount GlusterFS volumes and
    # start httpd if GLUSTER_IFACE is ready.
    # Replace eth0 with the proper interface name

    GLUSTER_IFACE=eth1
    export LC_ALL=C
    if [ "$DEVICE_IP_IFACE" == "$GLUSTER_IFACE" ]; then
        if [ "$2" == "up" ]; then
            mount -a -l -t glusterfs && systemctl start httpd
        fi
    fi
    exit 0

### Change to mpm-event
    sudo ls -l /etc/httpd/conf.modules.d/00-mpm.conf || sudo cp /etc/httpd/conf.modules.d/00-mpm.conf /etc/httpd/conf.modules.d/00-mpm.conf.bak
    sudo sed -i "$(sudo cat -n /etc/httpd/conf.modules.d/00-mpm.conf | grep mod_mpm_prefork.so | grep LoadModule | awk '{print $1}')"'s/^LoadModule/#LoadModule/g' /etc/httpd/conf.modules.d/00-mpm.conf
    sudo sed -i "$(sudo cat -n /etc/httpd/conf.modules.d/00-mpm.conf | grep mod_mpm_event.so | grep LoadModule | awk '{print $1}')"'s/#LoadModule/LoadModule/g' /etc/httpd/conf.modules.d/00-mpm.conf

### Allow Overrides in apache config
    sudo ls -l /etc/httpd/conf/httpd.conf.bak || sudo cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.bak
    sudo sed -i "$(sudo cat -n /etc/httpd/conf/httpd.conf.bak | grep -A50 'Directory "/var/www/html"' | grep AllowOverride | grep -v '#' | head -1 | awk '{print $1}')"'s/None/All/g' /etc/httpd/conf/httpd.conf

### Status page (Node 1 only)
    curl -L https://github.com/Humbedooh/server-status/archive/master.zip > /tmp/lua-status.zip
    sudo mkdir -p /var/www/status
    cd /var/www/status
    sudo unzip /tmp/lua-status.zip
    sudo mv server-status-master/server-status.lua .
    sudo sed -i 's/show_warning\ =\ true/show_warning\ =\ false/g' server-status.lua
    sudo rm -rf server-status-master
    
### Enable status config
    echo 'LuaMapHandler ^/server-status$ /var/www/status/server-status.lua' | sudo tee /etc/httpd/conf.d/status.conf

### Default php temp dir
    sudo mkdir /var/www/tmp
    sudo chmod 1777 /var/www/tmp
    echo 'sys_temp_dir=/var/www/tmp' | sudo tee /etc/php.d/97-tmpdirs.ini /etc/php-fpm.d/97-tmpdirs.ini /etc/php-zts.d/97-tmpdirs.ini 

### Default php session dir
    sudo mkdir -p /var/www/php/sessions
    sudo chmod 1777 /var/www/php/sessions
    echo 'session.save_path	=/var/www/php/sessions' | sudo tee /etc/php.d/95-sessiondirs.ini /etc/php-fpm.d/95-sessiondirs.ini /etc/php-zts.d/95-sessiondirs.ini 
    sudo sed -i 's/\/var\/lib\/php\/session/\/var\/www\/php\/sessions/g' /etc/httpd/conf.d/php*.conf

### Enable Apache remoteip settings (if apply)
    echo RemoteIPHeader X-Forwarded-For | sudo tee /etc/httpd/conf.d/remoteip.conf
    echo RemoteIPTrustedProxy  172.10.7.97 | sudo tee -a /etc/httpd/conf.d/remoteip.conf
    sudo sed -i 's/%h/%a/g' /etc/httpd/conf/httpd.conf

### Set time zone
    echo "date.timezone=$(sudo timedatectl | grep zone | awk '{print $3}')" | sudo tee /etc/php.d/95-timezone.ini /etc/php-fpm.d/95-timezone.ini /etc/php-zts.d/95-timezone.ini

### Add SFTP jail to webmaster group user
    sudo ls -l /etc/ssh/sshd_config.bak || sudo cp /etc/ssh/sshd_config  /etc/ssh/sshd_config.bak
    sudo grep apache /etc/ssh/sshd_config || cat <<EOF | sudo tee -a /etc/ssh/sshd_config
    Match Group apache
        ChrootDirectory /var/www
        PasswordAuthentication yes
        ForceCommand internal-sftp -u 002
        AllowTCPForwarding no
        PermitTunnel no
        X11Forwarding no
    EOF
    
### Change default umask on sftp system
    sudo grep 'sftp-server -u 002' /etc/ssh/sshd_config || sudo sed -i 's/sftp-server/sftp-server\ \-u\ 002/g' /etc/ssh/sshd_config  
    sudo systemctl restart sshd
    
### Create SFTP User
    sudo useradd -d /var/www/html webmaster -g "$(getent group apache | awk -F: '{print $3}')"
    sudo chown -R webmaster:apache /var/www/html
    sudo find /var/www/html -type d -print0 | sudo xargs -0 chmod 775
    sudo find /var/www/html -type f -print0 | sudo xargs -0 chmod 664
    echo webmaster:W3b-master | sudo chpasswd

### Start http server
    sudo systemctl start httpd

### Weekly html backup
    sudo mkdir -p /var/www/html_backup/$(hostname)
    cat <<EOF | sudo tee /etc/cron.d/html-weekly-backup
    ## HTML weekly backup
    $(shuf -n1 -i0-59) $(shuf -n1 -i0-5) * * $(shuf -n1 -i0-6)  root zip -r "/var/www/html_backup/$(hostname)/weekly.zip" /var/www/html  > /dev/null
    EOF
    
### Install HAProxy
#### TO-DO Upate to 17u
    sudo yum -y install haproxy socat
    sudo systemctl enable haproxy

### HAProxy config for MySQL
    sudo ls -l /etc/haproxy/haproxy.cfg.bak || sudo cp /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.bak
    sudo sed -i "$(cat -n /etc/haproxy/haproxy.cfg | grep 'to have these messages' | head -1 | awk '{print $1}')","$(wc -l /etc/haproxy/haproxy.cfg | awk '{print $1}')"d /etc/haproxy/haproxy.cfg
    grep MySQL /etc/haproxy/haproxy.cfg || cat <<EOF | sudo tee -a /etc/haproxy/haproxy.cfg
        log         127.0.0.1 local2
        chroot      /var/lib/haproxy
        pidfile     /var/run/haproxy.pid
        maxconn     10240
        ulimit-n    10240
        user        haproxy
        group       haproxy
        daemon
        # turn on stats unix socket
        stats socket /var/lib/haproxy/haproxy.sock mode 600 level admin

    #---------------------------------------------------------------------
    #  Status page - http://hostname:8080/
    #---------------------------------------------------------------------
    listen  stats 0.0.0.0:8080
        mode            http
        log             global
        maxconn         10240
        srvtimeout      100s
        contimeout      100s
        timeout queue   100s
        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:haproxy-St4tS
        stats uri  /
    
    #---------------------------------------------------------------------
    # MySQL Balancer - Listen on 127.0.0.1 port 3306
    #---------------------------------------------------------------------
    frontend mysql-local
        bind 127.0.0.1:3306
        bind /var/lib/mysql/mysql.sock mode 777
        mode tcp
        default_backend mysql-cluster
        maxconn         10240
        backend mysql-cluster
        balance leastconn
        mode tcp
        option tcpka
        maxconn 8400
        option mysql-check user haproxy
        server srvclrbd01 10.20.30.111:3306 maxconn 2800 check weight 1
        server srvclrbd02 10.20.30.112:3306 maxconn 2800 check weight 1
        server srvclrbd03 10.20.30.113:3306 maxconn 2800 check weight 1
    EOF

### HAProxy config for HTTP
    sudo ls -l /etc/haproxy/haproxy.cfg.bak || sudo cp /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.bak
    sudo sed -i "$(cat -n /etc/haproxy/haproxy.cfg | grep 'to have these messages' | head -1 | awk '{print $1}')","$(wc -l /etc/haproxy/haproxy.cfg | awk '{print $1}')"d /etc/haproxy/haproxy.cfg
    grep MySQL /etc/haproxy/haproxy.cfg || cat <<EOF | sudo tee -a /etc/haproxy/haproxy.cfg
        log         127.0.0.1 local2
        chroot      /var/lib/haproxy
        pidfile     /var/run/haproxy.pid
        maxconn     10240
        ulimit-n    10240
        user        haproxy
        group       haproxy
        daemon
        # turn on stats unix socket
        stats socket /var/lib/haproxy/haproxy.sock mode 600 level admin

    #---------------------------------------------------------------------
    #  Status page - http://hostname:8080/
    #---------------------------------------------------------------------
    listen  stats 0.0.0.0:8080
        mode            http
        log             global
        maxconn         10240
        srvtimeout      100s
        contimeout      100s
        timeout queue   100s
        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:haproxy-St4tS
        stats uri  /
    
    #---------------------------------------------------------------------
    # HTTP Balancer - Listen on port 80
    #---------------------------------------------------------------------
    frontend front_http
        bind :80
        mode http
        default_backend back_http

    backend back_http
        mode http
        option forwardfor
        balance roundrobin
        default-server inter 1s
        server srvclrapp01 172.10.7.90:80 check id 1
        server srvclrapp02 172.10.7.91:80 check id 2
    EOF

### Enable MySQL socket dir
    sudo mkdir -p /var/lib/mysql

### Start HAProxy
    sudo systemctl start haproxy


### Clean history
sudo rm -rf /root/.bash_history
rm -rf .bash_history ; history -c ; logout

### Reboot si aplica
sudo reboot; logout
   




# DB SERVERS ONLY

### Export path for db data volume
    export DBDATA=   # Block device here

### Export path for db binlog volume
    export DBBINLOG=   # Block device here

### Export path for db standard log volume
    export DBSTDLOG=   # Block device here

### Create volumes
    sudo mkfs.xfs -L DBDATA $DBDATA
    sudo mkfs.xfs -L DBBINLOG $DBBINLOG
    sudo mkfs.xfs -L DBSTDLOG $DBSTDLOG

### Yum variables
    # Export php variable manually can be
    # php56u php70u or php71u
    export IUSPHP=php71u
    # Export percona variable manually can be
    # 55 56 or 57
    export PERCONA=57

### Install app packages

    sudo yum -y install httpd httpd-manual httpd-tools mod_fcgid mod_ldap mod_proxy_html mod_session mod_ssl mod_xsendfile $IUSPHP $IUSPHP-bcmath $IUSPHP-cli $IUSPHP-common $IUSPHP-dba $IUSPHP-debuginfo $IUSPHP-embedded $IUSPHP-enchant $IUSPHP-fpm $IUSPHP-gd $IUSPHP-gmp $IUSPHP-imap $IUSPHP-interbase $IUSPHP-intl $IUSPHP-ldap $IUSPHP-mbstring $IUSPHP-mcrypt $IUSPHP-mysqlnd $IUSPHP-odbc $IUSPHP-opcache $IUSPHP-pdo $IUSPHP-pecl-imagick $IUSPHP-pecl-memcached $IUSPHP-pecl-redis $IUSPHP-pecl-xdebug $IUSPHP-pgsql $IUSPHP-process $IUSPHP-recode $IUSPHP-snmp $IUSPHP-soap $IUSPHP-tidy $IUSPHP-xml $IUSPHP-xmlrpc postfix Percona-XtraDB-Cluster-$PERCONA

### Enable firewall rules
    sudo firewall-cmd --zone=public --add-service=mysql
    sudo firewall-cmd --zone=public --add-service=mysql --permanent
    sudo firewall-cmd --zone=public --add-service=http
    sudo firewall-cmd --zone=public --add-service=http --permanent

### Move data to mysql Volumes
    sudo mkdir -p /var/lib/mysql-bin
    sudo mkdir -p /var/log/mysql
    sudo chown root: /var/lib/mysql
    sudo chown root: /var/lib/mysql-bin
    sudo chown root: /var/log/mysql


### Set the fstab
    grep DBDATA /etc/fstab || echo "LABEL=DBDATA /var/lib/mysql xfs defaults,noatime 0 0" | sudo tee -a /etc/fstab
    grep DBBINLOG /etc/fstab || echo "LABEL=DBBINLOG /var/lib/mysql-bin xfs defaults,noatime 0 0" | sudo tee -a /etc/fstab
    grep DBSTDLOG /etc/fstab || echo "LABEL=DBSTDLOG /var/log/mysql xfs defaults,noatime 0 0" | sudo tee -a /etc/fstab

### Mount all
    sudo mount -a
   
   
### Set permissions again
    sudo chown mysql: /var/lib/mysql
    sudo chown mysql: /var/lib/mysql-bin
    sudo chown mysql: /var/log/mysql


    
### BinLog config
    echo "[mysqld]" | sudo tee /etc/percona-xtradb-cluster.conf.d/mysqld_binlog.cnf
    echo "log-bin = /var/lib/mysql-bin/mysql-bin" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_binlog.cnf
    echo "max_binlog_size = 512M" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_binlog.cnf
    echo "max_binlog_files = 7" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_binlog.cnf
    

### Standard Log config
    sudo sed -i 's/^log-error/#log-error/g' /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
    sudo sed -i 's/^log-bin/#log-bin/g' /etc/percona-xtradb-cluster.conf.d/mysqld.cnf
    echo "[mysqld]" | sudo tee /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "log-error = /var/log/mysql/mysqld.log" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "general_log = false" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "general_log_file = /var/log/mysql/mysql-general.log" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "slow_query_log = true" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "slow_query_log_file = /var/log/mysql/mysql-slow.log" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "long_query_time = 2" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "log_queries_not_using_indexes = on" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf
    echo "log_slow_admin_statements = on" | sudo tee -a /etc/percona-xtradb-cluster.conf.d/mysqld_stdlog.cnf

### Basic tuning
    cat <<EOF | sudo tee /etc/percona-xtradb-cluster.conf.d/performance.cnf
    [mysqld]
    innodb_buffer_pool_size=1G
    innodb_buffer_pool_instances=10
    max_connections=2800
    thread_handling=pool-of-threads
    EOF
    
### Numa tuning
    cat <<EOF | sudo tee /etc/percona-xtradb-cluster.conf.d/numa.cnf
    [mysqld-safe]
    innodb_numa_interleave=on
    flush_caches=on
    EOF

### Kernel tuning
    echo 'vm.swappiness = 1' | sudo tee /etc/sysctl.d/97-swappiness.conf
    sudo sysctl --system

### Edit galera config

Run:

    sudo sed -i 's/^#wsrep_sst_auth/wsrep_sst_auth/g' /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
    sudo sed -i 's/^#wsrep_node_address/wsrep_node_address/g' /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
    sudo vim /etc/percona-xtradb-cluster.conf.d/wsrep.cnf
    
Edit (mannually) these lines with the proper ip addresses:

    wsrep_cluster_address=gcomm://192.168.70.71,192.168.70.72,192.168.70.73
    wsrep_node_address=192.168.70.71

### Run on Node 1

    sudo systemctl start mysql@bootstrap.service
    export MYPASS=$(sudo grep 'temporary password' /var/log/mysql/mysqld.log | tail -1 | awk '{print $NF}')

Log in MySQL cli

    mysql -u root -p"$MYPASS"
    
    
Run (one by one)

    ALTER USER 'root'@'localhost' IDENTIFIED BY 'r00t-Passw0rd';
    SHOW STATUS LIKE 'wsrep%'; 
    CREATE USER 'sstuser'@'localhost' IDENTIFIED BY 's3cretPass';
    GRANT PROCESS, RELOAD, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'sstuser'@'localhost';
    FLUSH PRIVILEGES;

### Run on Nodes 2 & 3 (one by one) 

    sudo systemctl start mysql

### And then Node 1 for clean restart

    sudo systemctl stop mysql@bootstrap.service
    sudo systemctl start mysql

### Enable all nodes
    sudo systemctl enable mysql
    

### Users only can be altered using DDL's
    CREATE USER 'dba'@'%' IDENTIFIED BY 'D4t4b4s3-admin';
    GRANT ALL PRIVILEGES ON *.* TO 'dba'@'%' WITH GRANT OPTION;
    
    CREATE USER 'demo'@'%' IDENTIFIED BY 'Dem0-user';
    CREATE DATABASE demo;
    GRANT ALL PRIVILEGES on demo.* TO 'demo'@'%';

### HAProxy user check the ip address
    CREATE USER 'haproxy'@'10.20.30.%';



    
### Backup unix / mysql user
    sudo useradd -m backup
    sudo gpasswd -a backup wheel
    
    CREATE USER 'mysql-backup'@'localhost' IDENTIFIED BY 'mysql-backup-S3cr3T';
    GRANT RELOAD, LOCK TABLES, REPLICATION CLIENT, CREATE TABLESPACE, PROCESS, SUPER, CREATE, INSERT, SELECT ON *.* TO 'mysql-backup'@'localhost';
    FLUSH PRIVILEGES;
    
    cat <<EOF | sudo tee /home/backup/.my.cnf
    [client]
    user=mysql-backup
    password=mysql-backup-S3cr3T
    EOF
    
    sudo chown backup: /home/backup/.my.cnf
    sudo chmod 400 /home/backup/.my.cnf
    
### Backup job
    sudo mkdir /home/backup/bin
    cat <<EOF | sudo tee /home/backup/bin/mysql-full-backup.sh
    #!/bin/bash
    set -eux
    ROTATIONDAYS=8
    BACKUPBASE="/backup/$(hostname)"
    BACKUPDIR="¬BACKUPBASE/¬(date +%Y%m%d-%H%M%S)"
    mkdir ¬BACKUPDIR
    sudo innobackupex --user=mysql-backup --password=mysql-backup-S3cr3T  --no-timestamp ¬BACKUPDIR
    sudo innobackupex --user=mysql-backup --password=mysql-backup-S3cr3T  --apply-log ¬BACKUPDIR
    BINLOGPOS="¬(cat ¬BACKUPDIR/xtrabackup_binlog_info | awk '{print ¬1}')"
    echo "PURGE BINARY LOGS TO '¬BINLOGPOS';" | mysql
    find "¬BACKUPBASE" -mtime +¬ROTATIONDAYS -exec rm -rf {} \;
    EOF
    sudo sed -i 's/¬/$/g' /home/backup/bin/mysql-full-backup.sh
    sudo chmod 755 /home/backup/bin/mysql-full-backup.sh
    sudo chown -R backup: /home/backup

### Backup cronjob
    cat <<EOF | sudo tee /etc/cron.d/mysql-full-backup
    ## MySQL daily backup
    $(shuf -n1 -i0-59) $(shuf -n1 -i0-7),$(shuf -n1 -i8-15),$(shuf -n1 -i16-23) * * * backup /bin/bash ~/bin/mysql-full-backup.sh
    EOF
    

### Backup filesystem
    sudo yum -y install cifs-utils
    grep cifs /etc/fstab || echo "//172.10.7.95/backup /backup cifs user=mysql-backup,password=mysql-backup-S3cr3T,_netdev,uid=backup,gid=backup 0 0" | sudo tee -a /etc/fstab
    sudo mkdir -p /backup
    sudo mount -a



### AS BACKUP USER
    mkdir -p /backup/"$(hostname)"/

### mysql-backup mysql-backup-S3cr3T


### Log rotation
    sudo mkdir /home/backup/bin
    cat <<EOF | sudo tee /home/backup/bin/mysql-log-rotate.sh
    #!/bin/bash
    set -eux
    sudo rm -f /var/log/mysql/mysql*log
    mysqladmin flush-logs
    EOF
    sudo sed -i 's/¬/$/g' /home/backup/bin/mysql-log-rotate.sh
    sudo chmod 755 /home/backup/bin/mysql-log-rotate.sh
    sudo chown -R backup: /home/backup
    

### Log rotation cronjob
    cat <<EOF | sudo tee /etc/cron.d/mysql-log-rotate
    ## MySQL weekly log rotation
    $(shuf -n1 -i0-59) $(shuf -n1 -i0-23) * * 0  backup /bin/bash ~/bin/mysql-log-rotate.sh
    EOF

### Clean useless logs
    sudo rm /var/log/mysqld.log

### Restart cron
    sudo systemctl restart crond

### Install phpmyadmin
    cd /var/www/html
    sudo wget https://files.phpmyadmin.net/phpMyAdmin/4.7.6/phpMyAdmin-4.7.6-all-languages.tar.xz
    sudo tar xvJf phpMyAdmin-*
    sudo rm phpMyAdmin-*.tar*
    sudo mv phpMyAdmin-* phpmyadmin
    cd /var/www/html
    echo "<?php" | sudo tee phpmyadmin/config.inc.php
    echo "\$cfg['PmaNoRelation_DisableWarning'] = true;" | sudo tee -a phpmyadmin/config.inc.php
    echo "\$cfg['blowfish_secret'] = '$(pwgen 32)';" | sudo tee -a phpmyadmin/config.inc.php
    echo "\$cfg['Servers'][1]['verbose'] = '$(hostname)';" | sudo tee -a phpmyadmin/config.inc.php
    cd
    #end
    
### Start phpmyadmin - apache
    sudo systemctl enable httpd
    sudo systemctl start httpd
   
### Clean history
sudo rm -rf /root/.bash_history
rm -rf .bash_history ; history -c ; logout

### Reboot si aplica
sudo reboot; logout
   
   
    
### Falta el tuning general + backups
https://www.percona.com/doc/percona-xtrabackup/LATEST/innobackupex/restoring_a_backup_ibk.html
https://dev.mysql.com/doc/mysql-backup-excerpt/5.7/en/backup-policy.html
https://www.percona.com/blog/2013/04/18/rotating-mysql-slow-logs-safely/

### Ayuda
https://www.percona.com/blog/2016/05/03/best-practices-for-configuring-optimal-mysql-memory-usage/
http://php.net/manual/en/datetime.configuration.php#ini.date.timezone
https://www.percona.com/doc/percona-xtradb-cluster/LATEST/features/pxc-strict-mode.html
https://www.percona.com/blog/2009/04/15/how-to-decrease-innodb-shutdown-times/
