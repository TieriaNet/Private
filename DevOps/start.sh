#!/bin/bash
#
# Install common DevOps tools
set -Eeuxo pipefail
BIN_DIR="${HOME}/.local/bin"

PACKAGES=(coreutils curl elinks gdebi-core sudo python3-pip unzip)
PACKAGE_ARCHIVE_DIR="/tmp/$(id -u)-installfiles"
PLATFORM="$(uname -i)"

HASHICORP_BASE_URL="https://releases.hashicorp.com"
PACKER_URL="${HASHICORP_BASE_URL}/packer"
VAGRANT_URL="${HASHICORP_BASE_URL}/vagrant"

main() {
    mkdir -p "${BIN_DIR}"
    mkdir -p "${PACKAGE_ARCHIVE_DIR}"
    install_dependencies
    install_ansible
    install_vagrant
    install_packer
}

install_ansible() {
    pip3 install --upgrade ansible
}

install_dependencies() {
    sudo apt-get install "${PACKAGES[@]}"
}

install_vagrant() {
    local format latest_dir latest_version
    format="deb"
    latest_dir="$(links -dump "${VAGRANT_URL}" | grep '\*' | awk '{print $2}' \
                | sort -n | tail -1 | awk -F_ '{print $2}')"
    latest_version="$(links -dump "${VAGRANT_URL}/${latest_dir}/" \
                    | grep "${PLATFORM}" | grep "${format}" \
                    | awk '{print $2}')"
    cd "${PACKAGE_ARCHIVE_DIR}"
    wget -c "${VAGRANT_URL}/${latest_dir}/${latest_version}"
    sudo gdebi --n "${latest_version}"
}

install_packer() {
    local format zip_url zip_package kernel nice_arch
    format="zip"
    case ${PLATFORM} in
        x86_64)    nice_arch="amd64" ;;
        i[3456]86) nice_arch="i386" ;;
        *)         nice_arch="${PLATFORM}" ;;
    esac
    kernel="$(uname -s | tr "[:upper:]" "[:lower:]")"
    latest_dir="$(links -dump "${PACKER_URL}" | grep '\*' | awk '{print $2}' \
                | sort -n | tail -1 | awk -F_ '{print $2}')"
    latest_version="$(links -dump "${PACKER_URL}/${latest_dir}/" \
                    | grep "${nice_arch}" | grep "${format}" \
                    | grep "${kernel}"| awk '{print $2}')"
    cd "${PACKAGE_ARCHIVE_DIR}"
    wget -c "${PACKER_URL}/${latest_dir}/${latest_version}"
    unzip -o "${latest_version}" -d "${BIN_DIR}"
}


main
