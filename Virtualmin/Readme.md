## Virtualmin-NG


### Deployment URL's
[Drive Link](https://drive.google.com/file/d/0Bx6obLXjBnCbUHBzVzg4UVFvUkE/view)
[Short Link](https://drive.google.com/uc?export=download&id=0Bx6obLXjBnCbUHBzVzg4UVFvUkE)

### Quick Start

``` bash
 unset HISTFILE
 curl -sL https://goo.gl/MfQUND | bash
 history -c; rm .bash_history;
 clear; logout
```

### Known problems
- MySQL module doesn't work with Google Cloud SQL

### TO-DO
- Crontab cleaner for /etc/nginx/conf.d/*.lock
- https://perishablepress.com/expose-php/
- https://github.com/iuscommunity/yum-plugin-replace
- http://man7.org/linux/man-pages/man8/yum-cron.8.html
- Backups
- Enable Apache/NGINX mod-status: https://httpd.apache.org/docs/2.4/mod/mod_status.html http://nginx.org/en/docs/http/ngx_http_status_module.html
- Apache Fix: find -name .htaccess | xargs sed -i 's/FollowSymLinks/SymLinksIfOwnerMatch/g'                    

## NGINX Specific site settings
Edit tge config file in: Webmin >  Servers > Nginx Webserver > Edit Configuration Files, then reload the nginx service for apply the changes.

Disable .htaccess and other hidden files

```
location  /. {
	return 404;
}
```

Clean / Friendly URIs

```
location / {
	try_files $uri /index.php?$query_string;
}
```

Force https

```

if ($https = "") {
	rewrite  (.*)  https://$http_host$1;
}
```

Protect some locations

```
location ~ ^/sites/.*/private/ {
	return 403;
}
```

Wordpress

```
	# Force https
	if ($https = "") {
		rewrite  (.*)  https://$http_host$1;
	}
    
	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}
	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}
	location / {
	# This is cool because no php is touched for static content.
	# include the "?$args" part so non-default permalinks doesn't break when using query string
	try_files $uri $uri/ /index.php?$args;
	}
	location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
		expires max;
		log_not_found off;
	}
```

## Magento tips

- Minimal memory: 2GB RAM minimo
- PHP: All timeouts to 180
- Apache: Change FollowSymLinks to SymLinksIfOwnerMatch as option.

```
    1,$ s/,SymLinksIfOwnerMatch/,SymLinksIfOwnerMatch,FollowSymLinks/g
```
- MySQL time/date must be on sync with php timezone.
