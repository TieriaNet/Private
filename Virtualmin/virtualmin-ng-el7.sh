#!/bin/bash
# Version 0.12Final
# Tue Apr 11 02:11:47 COT 2017

set -eu

###########
# Globals
#

# Get OS Info
source /etc/os-release
VERSION_ID=$(printf $VERSION_ID | cut -c1)
export APACHENAME='httpd'
export NGINXNAME='nginx'
export RELEASE='0.12'

# Set http server to run between httpd or nginx
export HTTPSERVER=$(whiptail --title "HTTP Server" --nocancel --menu "Choose an option" 15 50 2 "$APACHENAME" "Apache 2.4" "$NGINXNAME" "Nginx 1.10" 3>&1 1>&2 2>&3)

# Set default php version (5.6/7.0/7.1)
export IUSPHP=$(whiptail --title "PHP version" --nocancel --menu "Choose an option" 15 50 3 "php56u" "Version 5.6" "php70u" "Version 7.0" "php71u" "Version 7.1" 3>&1 1>&2 2>&3)

main() {
    os_tweaks
    yum_repokeys
    rpm_install
    virtualmin_cfg
    apache_cfg
    nginx_cfg
    php_cfg
    system_srv
    finish
}

###########################
# Operating System tweaks
#
os_tweaks() {
    # Disable SELinux
    /sbin/getenforce | grep -i disabled || /sbin/setenforce 0
    sed -i s/SELINUX=enforcing/SELINUX=disabled/g /etc/sysconfig/selinux /etc/selinux/config
}

#################################
# Additional YUM repos and keys
#
yum_repokeys() {
    # Virtualmin-NG
    test -f /etc/pki/rpm-gpg/RPM-GPG-KEY-virtualmin || curl -s  http://software.virtualmin.com/lib/RPM-GPG-KEY-virtualmin > /etc/pki/rpm-gpg/RPM-GPG-KEY-virtualmin
    test -f /etc/pki/rpm-gpg/RPM-GPG-KEY-webmin     || curl -s  http://software.virtualmin.com/lib/RPM-GPG-KEY-webmin     > /etc/pki/rpm-gpg/RPM-GPG-KEY-webmin
    file_eyv | decode > /etc/yum.repos.d/virtualmin.repo

    # EPEL
    rpm -q epel-release || yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-$VERSION_ID.noarch.rpm || yum -y install https://mirrors.kernel.org/fedora-epel/epel-release-latest-$VERSION_ID.noarch.rpm

    # IUS
    rpm -q ius-release  || yum -y install https://$ID$VERSION_ID.iuscommunity.org/ius-release.rpm

    # RHEL Optional RPMS (RHEL Only)
    if [ "$ID" == "rhel" ]; then
        subscription-manager repos --enable=rhel-$VERSION_ID-server-optional-rpms
    fi
}


#########################
# Software installation
#
rpm_install() {
    # System update
    yum -y update

    # Install Virtualmin and deps
    yes | yum -y install \
    bash-completion bzip2 expect fail2ban ftp htop iptraf-ng lftp lsof mailx mariadb mc nano nc ncftp nmap ntpdate p7zip pwgen rdate sudo sharutils sysstat tcping tmux unzip vim vnstat wget yum-cron yum-plugin-replace yum-versionlock zip \
    httpd httpd-manual httpd-tools mod_fcgid mod_ldap mod_proxy_html mod_session mod_ssl mod_xsendfile webalizer \
    nginx-all-modules nginx-filesystem nginx-mod-http-image-filter nginx-mod-http-perl nginx-mod-http-xslt-filter nginx-mod-mail nginx-mod-stream \
    $IUSPHP-bcmath $IUSPHP-cli $IUSPHP-common $IUSPHP-dba $IUSPHP-debuginfo $IUSPHP-embedded $IUSPHP-enchant $IUSPHP-fpm $IUSPHP-gd $IUSPHP-gmp $IUSPHP-imap $IUSPHP-interbase $IUSPHP-intl $IUSPHP-ldap $IUSPHP-mbstring $IUSPHP-mcrypt $IUSPHP-mysqlnd $IUSPHP-odbc $IUSPHP-opcache $IUSPHP-pdo $IUSPHP-pecl-imagick $IUSPHP-pecl-memcached $IUSPHP-pecl-redis $IUSPHP-pecl-xdebug $IUSPHP-pgsql $IUSPHP-process $IUSPHP-recode $IUSPHP-snmp $IUSPHP-soap $IUSPHP-tidy $IUSPHP-xml $IUSPHP-xmlrpc \
    perl-DBD-MySQL perl-DBD-SQLite ust-virtual-server-mobile ust-virtual-server-theme virtualmin-release wbm-security-updates wbm-virtualmin-htpasswd wbm-virtualmin-init wbm-virtualmin-nginx wbm-virtualmin-nginx-ssl wbm-virtual-server wbt-virtual-server-mobile wbt-virtual-server-theme
}


############################
# Virtualmin configuration
#
virtualmin_cfg() {
    # Virtualmin defaults
    file_ewvc | decode > /etc/webmin/virtual-server/config

    # Default shell for users
    grep '/bin/bash' /etc/webmin/useradmin/config || echo 'default_shell=/bin/bash' >> /etc/webmin/useradmin/config

    # SFTP Settings
    grep 'sftponly' /etc/ssh/sshd_config || file_ess | decode >> /etc/ssh/sshd_config
    getent group sftponly || groupadd -r sftponly

    # Settings for virtualmin apache
    if [ "$HTTPSERVER" == "httpd" ]; then
        file_ewvc_af | decode >> /etc/webmin/virtual-server/config
    fi

    # Settings for virtualmin nginx
    if [ "$HTTPSERVER" == "nginx" ]; then
        file_ewvc_nf | decode >> /etc/webmin/virtual-server/config
    fi

    # Virtualmin Nginx module config
    file_ewvnc | decode > /etc/webmin/virtualmin-nginx/config

    # Virtualmin Apache module config
    grep '/etc/httpd/conf.d/' /etc/webmin/apache/config || echo 'virt_file=/etc/httpd/conf.d/' >> /etc/webmin/apache/config
    sed -i 's/test_manual=0/test_manual=1/g' /etc/webmin/apache/config
    sed -i 's/test_apachectl=0/test_apachectl=1/g' /etc/webmin/apache/config
    sed -i 's/test_config_manual=0/test_config_manual=1/g' /etc/webmin/apache/config

    # Virtualmin backup template
    mkdir -p /etc/webmin/virtual-server/backups
    file_ewvb1| decode > /etc/webmin/virtual-server/backups/100000000000000
    file_ewvb2| decode > /etc/webmin/virtual-server/backups/200000000000000

    # System backups
    mkdir -p /backup/Config/
    echo '@daily  root /usr/bin/tar cJf /backup/Config/etc-$(date "+\%A").tar.xz /etc --selinux --xattrs 2>/dev/null' > /etc/cron.d/etc-backup

    # Webmin unix user module config
    sed -i "s/base_uid=500/base_uid=1001/g" /etc/webmin/useradmin/config
    sed -i "s/base_gid=500/base_gid=1001/g" /etc/webmin/useradmin/config

    # Webmin base config
    sed -i "s/apache=0/apache=1/g" /etc/webmin/installed.cache
    grep ssl_redirect /etc/webmin/miniserv.conf || echo "ssl_redirect=1" >> /etc/webmin/miniserv.conf

    # Webmin-Extended routines
    mkdir -p /usr/libexec/webmin-extended
    file_ulwvpa | decode > /usr/libexec/webmin-extended/virtualmin-patch.sh
    file_ulwvpr | decode > /usr/libexec/webmin-extended/virtualmin-pre.sh
    file_ulwvpo | decode > /usr/libexec/webmin-extended/virtualmin-post.sh
    chmod +x /usr/libexec/webmin-extended/*.sh
    echo '*/5 * * * * root /usr/libexec/webmin-extended/virtualmin-patch.sh' > /etc/cron.d/virtualmin-patch
    /usr/libexec/webmin-extended/virtualmin-patch.sh

    # Firewalld config
    firewall-cmd --set-default-zone=public
    firewall-cmd --zone=public --add-masquerade
    firewall-cmd --zone=public --add-masquerade --permanent
    firewall-cmd --zone=public --add-forward-port=port=2222:proto=tcp:toport=22
    firewall-cmd --zone=public --add-forward-port=port=2222:proto=tcp:toport=22 --permanent
    for FIREWALL_SERVICE in http https ssh; do
        firewall-cmd --zone=public --add-service=$FIREWALL_SERVICE
        firewall-cmd --zone=public --add-service=$FIREWALL_SERVICE --permanent
    done
    for FIREWALL_PORT in 2222/tcp 10000/tcp; do
        firewall-cmd --zone=public --add-port=$FIREWALL_PORT
        firewall-cmd --zone=public --add-port=$FIREWALL_PORT --permanent
    done
}


#########################
# Apache HTTPD settings
#
apache_cfg(){ 
    # Use MPM Event as default
    sed -i 's/^LoadModule/#LoadModule/g' /etc/httpd/conf.modules.d/00-mpm.conf
    sed -i 's/#LoadModule\ mpm_event_module/LoadModule\ mpm_event_module/g' /etc/httpd/conf.modules.d/00-mpm.conf

    # Performance configuration
    file_ehcp | decode > /etc/httpd/conf.d/performance.conf

    # Default VirtualHost
    file_ehc0 | decode > /etc/httpd/conf.d/000-default.conf
    file_vwhi | decode > /var/www/html/index.html

    # Hide http headers
    echo '# This directive controls whether Server response header field which is sent back to clients' > /etc/httpd/conf.d/security.conf
    echo 'ServerTokens ProductOnly' >> /etc/httpd/conf.d/security.conf
}

##################
# NGINX settings
#
nginx_cfg() {
    # Default VirtualHost
    mkdir -p /var/www/nginx
    sed -i 's/\/usr\/share\/nginx\/html/\/var\/www\/nginx/g' /etc/nginx/nginx.conf
    file_vwni | decode > /var/www/nginx/index.html

    # Performance configuration
    file_encp | decode > /etc/nginx/conf.d/performance.conf

    # Default SSL VirtualHost
    egrep '^#.*listen.*443.*' /etc/nginx/nginx.conf && echo "sed -i '""$(($(grep -n  TLS /etc/nginx/nginx.conf | awk -F: '{print $1}') + 1))"",$ s/#//'" /etc/nginx/nginx.conf  | bash
    sed -i 's/\/etc\/pki\/nginx\/server.crt/\/etc\/pki\/tls\/certs\/localhost.crt/g' /etc/nginx/nginx.conf
    sed -i 's/\/etc\/pki\/nginx\/private\/server.key/\/etc\/pki\/tls\/private\/localhost.key/g' /etc/nginx/nginx.conf

    # Hide http headers
    echo '# Disables emitting nginx version on error pages and in the -Server- response header field.' > /etc/nginx/conf.d/security.conf
    echo 'server_tokens off;' >> /etc/nginx/conf.d/security.conf

    # Set server settings
    echo '# Sets the maximum allowed size of the client request body' > /etc/nginx/conf.d/server.conf
    echo 'client_max_body_size 1G;' >> /etc/nginx/conf.d/server.conf  
}


################
# PHP settings
#
php_cfg() {
    # Hide Apache/NGINX/PHP version headers
    sed -i 's/expose_php\ =\ On/expose_php\ =\ Off/g' /etc/php.ini

    # PHP-FPM default worker
    rm -rf /etc/php-fpm.d/www.conf 
    file_epd | decode > /etc/php-fpm.d/default.conf
}

#######################
# Additional services
#
system_srv() {
    # Enable default http server
    systemctl stop httpd
    systemctl stop nginx
    systemctl disable httpd
    systemctl disable nginx
    systemctl enable $HTTPSERVER
    systemctl start  $HTTPSERVER

    # Re-Start OpenSSH
    systemctl enable sshd
    systemctl restart sshd

    # Start SysStat
    systemctl enable sysstat
    systemctl start sysstat

    # Start VnStat
    echo "for IF in $(ip link show | egrep '^[0-9]:' | awk -F: '{print $2}' | tr \\n ' ' | sed -e s/lo//); do vnstat -u -i \$IF; done" | bash; vnstat
    chown -R vnstat: /var/lib/vnstat
    systemctl enable vnstat
    systemctl start vnstat

    # Start Fail2ban
    systemctl enable fail2ban
    systemctl start fail2ban

    # Additional Cron jobs
    echo '35 2 * * * root ((/usr/sbin/ntpdate -u pool.ntp.org 2>&1 || /usr/bin/rdate -ps time.nist.gov 2>&1) | /usr/bin/logger -t date-time) && (/sbin/hwclock --systohc 2>&1 | /usr/bin/logger -t date-time)' > /etc/cron.d/datetime
    systemctl restart crond

    # Write Virtualmin-NG version
    test -f /etc/webmin/virtualmin-ng-release || echo "VIRTUALMIN-NG-RELEASE=$RELEASE" >> /etc/webmin/virtualmin-ng-release

    # Restart Webmin
    service webmin restart

    #Disable unnecessary services
    systemctl disable usermin
    systemctl stop usermin
}


###############
# Final steps
finish() {
    # Check virtualmin config
    virtualmin check-config

    # Login URL's
    echo "WebPanel ready in:"
    ip addr | grep inet | grep -v inet6 | awk '{print $2}' | awk -F/ '{print "https://"$1 ":10000"}'
    echo "https://$(curl -s icanhazip.com):10000"

    # Clear history
    rm -rf ~/.bash_history
    touch  ~/.bash_history
    history -c
    history -w

    # End
    echo;echo;echo "All Done!"
}


####################
# Special routines
decode() {
    base64 -d | gunzip
}


#################
# Encoded files
#

# File: /etc/yum.repos.d/virtualmin.repo
file_eyv() {
    printf 'H4sIALI8+FgAA8WQT0sDMRDF7/kUOfSaDl4LAUHLClZdWhCkesims9lhs9mQPy1+e1Ncuz2KFwOBxwzv95K3P1JIWdmB3AdzakC5fVhv4A5detnBTlMR1JLmi4AWVcQjBi74e1OkCror+vVCYOdpDlZ2KfkVQFVvVuXexrFNJxVwOYct9TiA8RZChxau4LD4QQNDpxqLB3nDjDc9fsqWLBYuYNLge4LgB1FWsK2fRFVX4nH9JuYMxr/Pb1wnbM6OMtYd6r5EktM2H9D3JsqZKaaX8snA9le77Kj8ICo7VTk3w+8ppkBNTjQ6/ow5BWV5rXSvDMa/9HbJ+ueavgAxKuXTQQIAAA=='
}

# File: /etc/webmin/virtual-server/config
# egrep -v '^php[+0-9]|last_check|bind_master|plugins_inactive' 
file_ewvc() {
    printf 'H4sIAKkbAFkAA8VYW2/bOBZ+tn6FH7ovHSWyc2u3qAIUbbYToJkM2k533gRKoi1uJFElKTvuoP99v3NISY6dxWKfFglsno/3c/1oUSthC93t0mUkqJ01upSj4HadtOkyPosv46v4Vfw6/nu8XMTLZbw8i5fnGFVnrWjkRhnXizpdEKK3tEhXdaNo+7zUjZ3kXVOr9mEC+q6ThqROFJXMCt2u1Dr9Is1Gmt+w/vzFXx/u737OPPKOzjbfbrenAf6gi76RrfustcPQX+/vbn4mXZ/Xqsgq19SzG2O0+aTX82QjTFLrdRJO3Kg28YtkksZk6Ju9763TzX8ZLopCWkvj54VuctXKcvalMKpz/nhJsVYngJPxQAMw+6CMLJw2u9u2lI9zRZ+ndM6pGVpQ4tS6mJqXs7fjIs9d+Hp23zmlWzs/4T2knf9y2xZ1X0r72/3Nnzfv57982TWfyAi3q/ttK82dcEU1Y3PMV0Y3czRn70i8h8qNKuUc0jysm948yuL9x9t4WDV+unwcto3v+tqpb0pubfzshm+T8SLXz10qKO36/3uywTGtEyY9g9TJtqQo4QbgXS3JfXuns1o6K9vC7Do3QEaWvBTJG6HqTDiazE1crnw9SkUl2rU86S3CYcSMbieBfXNcqMSFh66VquUTAd46ypXzDntSuU5Yuy3HngYfT4RcP8pp1WZnv0/9B3PhiqpVk6itW5u9yZ3RBemLBVvJuh5PbjvRjOPsziKQRtHJupWTivqu1Nt2nDnF4onYwiLOPtdVik16PPJ4XLtW7eO4wFbm47ZoIwv+2LMEEEyhk2L81mZFU6b4jnJRPPRdtpLC9UZm8E9MOQDhAc+AbL4DEOsbjcPK465gq0M0mOgA3rPGQY+1zwzvW/V4jE6aOu7zyjoGR60NXY2DE3gBl8tKsbPpq6FXtzLf4WMa729fQsk5RSICQAZtFIX1X06nLeZEFD1Z2QhTdAiv5WKxj+wNsd1KsPMNUrqTNgh97oVt5venHbbZyvFZh7MwRuon61NQk9xqQnRPgc2iU6tdenZBgqZM4qehtildpuc8yMmmq+k6pVwJpKCoEO1BziiQbXJVK6dgPL5DIY3jYpzaSpxFSEXFQ+ZzErYoatFkpawV0uEuTUq5Sdq+rhm3tDz56SBEha5r5KJMtQ7VFEX7coRa7ZU0yHAeJDwKRJS4DpJFhkV/ma9abSRVfpJwESp5VOJr1SiXvozC5TgBBKcFdHwxgCttCtkjKRu/lgJvOOU/L12lb95Qq9EtvLkOW1ww9L2HedLL5dmCB7e6zKdD+c7l4uL15asrAuBG9cEhcSfdY//pOD2Cr+8m8WiV3uexMACWk1BlyTk5iHARO0kUQV6kWgGW4qMYADLCOT6tyGs5gSymFI0xYTECKkaeiDnK4yGoY/hnROljQZ8Ze0Rod0ZxWinJOyH7C1lM0G29G2GrYVk4fwAmx/Q3AzhSQFpYb2ShXUgdpRErZ7OVrslsH1iKUORcJqjw0O4sYc+hWelG0mlZCEqNQtJggwQTrJSxLoMns69dni5eRewhmQwaIgpAngkjsMvQHvwZCkxCHAseaWW0FkXtr2/TF399/Hz/x+8/PWjAFNPAMTzSPzuuD3R4LRHNqqAL8Dh/A5t6yZLPYdAP1Y3JuEJMOiO6bFspRxqshCmHadRtKyqm3GErT7RfRhXYTMaKpw6obMhJexXLI0OvlSmzVNDhxGIn6zuG87HaufgQp0An2CElJGLjry4XkVqJQqay7Rb2PGJ+CfZd2xT2j2lGzHks9pN9/4oHwBG8SwKF8fp8cqAlQ5OMXborZAv4dckC/AbsDc0HucssikV6hviK6lKQKelrCIexDdZjZPqCDJbcQcaNEt8b6lYtxQaqK3gW+RMlM6/HWheiPvDvqcgupjY8SICrcTmokW5ab9daaypLIU6gCcVPnXDEUBT2sjwhowGawG+wPQ9+DPSAWt6SFlmApEa0/g3VyCYj/ZxdXkXNY4YcrUqvV+9cC/9NZNFY8CS+0ACtTTcMJEtONx6gtrX7QJnD5X//fPOP2z9/BqhCjrHp34LUPJT5uGKrg9MOIjPVQbT9agVjjOtlAe/3diWd4qqPMHloU4ShLbdQsPf9LNd9WxyiCBDKfo2ALg02ubl7d/vp6/3PGdGyI1DkONo+UD03d9gA/vsvIujffJjNLT805555lDQKJJQ8BGw+cGTohGCMPABxn1bJ0tqKpeyp+5RCNszntaswNbyLvcBRRlLvut7BVg18gkIGQcjFmGJgEXkSPrzUuxxp54zgqisqRAYKc3pFEsxD2dZ34Yp49hAbRxS0GVEYmlz3a6oWEWk2VJFlEMLuSW/xBlY5zU48Az6Rj45qWbn3Lj6hSae24snwglApRhK6GJsZmI4uVbv2DjHAnDVBMv73jY3kfVFHtCgHfwLzQE0o//NvCW/fYftdAw8eH5uYcD37A0aYozX7SKmdWwT5V70Y5xD+9hMlmvk/P99+vbmefZDtDg9RPBwZvp59lt97vPu+Ufx+odo016vV7CZcgX5fGDYmxkC7n9LPD2+T8WTXdI3HXRaCjnPKoJ8RoHIy1MODLGdI2WWnwfXSyrnOvkkSRRxEud2p6NSpQQbCS6yQBWhseYqlk80SzAsTLT3B0Orz3eA7J8vI8k8ctANcZMjpgInXBDrwBe0o1OKRi9pKb4c3Ap2c5RrhiBurdgA401M9CHK7Sn3Ixj56PMraWIa2kRu84YcJQSFBQtpV7UrD51l0Ih+7As0bpKfzQsb1ZJcfqv6rQJExXrMkZkWtcNeU2jgS/umE3T4Zf1qwTqkz8UNQlB7Csp4n5LUYEQL29hkoBIUUPukhm1VU08fHVgBDwC+DOCgKQm/DzN4SCxwFLEmPCxrFP9IdM8Fg+2U0wEPucLUVyEqFNuyLDr3cFSKOJzOIdEY58LjTgPkMXvOVhIiL+Tl/7dM50Jxqzi0aEzLx4UOKHpeIf2fodYE1ELQN1WvOQ6O0VXhS4rg+cT+5KeUUDKWvq/CNFJPZB4ljSFck1HoCU92guwNjBW863XHBZxX5BEbkg49OGJwfLLycBpIVmb8B13Uy4NMA76ebonrothPaeI9Kn471b869Y3B1HtfcCtM+9wKld/yCPjNRNhyM0yN+rw3v4p+iGBrHTSoOAioZWNNLXq+jN8ZrvzaiA4HoWzV3XFycs8yn9K9ZlrmeM7faqh/Emk1PBfPfBZfi7awWAAA='
}

# SSHD fragment: /etc/ssh/sshd_config
file_ess() {
    printf 'H4sIAENV8FgAA1WOQQrCQAxF9z1FwLWUHqFU6kahYBduh2m0g9OkJKlDb+9YdeFf5j/yfrGDE/sHXNq+AyZQczQ4GWBRFC3OzvwIR+FlBr3ZzBTXAnKaUZjtEAS9saxQPp2UKaVSg6FuSOdUE8tQLzYiWfDOQjas37pl8djwNGUhBDIUcnH/lmx1HSOnvukylvKeQHcg/vxFmYL1CxHG3+1aVf/gC3evNNDYAAAA'
}

# Apache fragment: /etc/webmin/virtual-server/config
file_ewvc_af() {
    printf 'H4sIAPxG+FgAA8vJTy/KL0ksSbU15sqtLC7MsTXgKi7OsTXiKk9NsjUEkYk5mVWpRUA2AHXnzF4sAAAA'
}

# Ngix fragment: /etc/webmin/virtual-server/config
file_ewvc_nf() {
    printf 'H4sIAE/H5lgAA8vJTy/KL0ksSbU15sqtLC7MsTXgKsgpTc/MK47PzEtMLsksS7UtyywqKU3Myc3M080DylToFhfnwFRhlVRAF+QqT01KzMmsSi2yNeQCADbhOZp0AAAA'
}

# File: /etc/webmin/virtualmin-nginx/config
file_ewvnc() {
    printf 'H4sIAO1y7FgAA3WN0Q3CMAxE/zNFJ6jFAJklSh1ToraxFRtEt4cGoSLUfvp8715MKRh7IEMoYy5PQC7XPoGLIvMacEke7lphyAV0VaMFbe4qqcVqXUN6pfrISK5dO6Ib07Lv5z2dx1/Zh99yJzcJyjiR+Ytr62fyI7Uay3mf5a/+Al0q+tr3AAAA'
}

# File: /usr/libexec/webmin-extended/virtualmin-patch.sh
file_ulwvpa() {
    printf 'H4sIAJae9VgAAz2OvQrCMACE9z7FiUMsWKNFcHDRSQqlCAVxCEhSYxuIseRH27c3DnW64/iOu/mMCmWo4K5LkjnO3DcdRGjbEXVV4KKsD1w/lUFdl6hORXWFVsJyO0a8D65bHJzTr967JchdPnjQnkQbQ5LukaFURiLfbLE49hb5erNLEyfvyBQcZVOFkSUD+7UYoZOCBmdpnJODbOhHiviDvv+XMtMqM2SRncLbQ3IfrFz1OvkCnBUKl9oAAAA='
}

# File: /usr/libexec/webmin-extended/virtualmin-pre.sh
file_ulwvpr() {
    printf 'H4sIANSr9VgAA02MMQ7CMAxF95zCVWcwHIGBHQEXcJIvxVJrqjiBcnsKE9vTk94bB45qHMVLCCPd7ucLHTa4dqNFWipwUiOxNyVxBO5eedKIFYlfiLPaDmuDZWR+am1dpq/7tfu/6XGDU840PyrIGxangoohfACpJNX4ggAAAA=='
}

# File: /usr/libexec/webmin-extended/virtualmin-post.sh
file_ulwvpo() {
    printf 'H4sIANSr9VgAA02MMQ7CMAxF95zCVWcwHIGBHQEXcJIvxVJrqjiBcnsKE9vTk94bB45qHMVLCCPd7ucLHTa4dqNFWipwUiOxNyVxBO5eedKIFYlfiLPaDmuDZWR+am1dpq/7tfu/6XGDU840PyrIGxangoohfACpJNX4ggAAAA=='
}

# File: /etc/webmin/virtual-server/backups/100000000000000
file_ewvb1() {
    printf 'H4sIAHu/9VgAA12QP2/EIAzFd38PtlbkburiterSqVOniISXKwp/IgPX3revc7mlRcIy2O/nBxApUvlMHrWxndy89s2+9hit+TTJ+NG8mXfzQdcgrbuYQmZaQgRbtNl+Y9Ib+yg+V8gV8qBUexr+LFpDjDyQ03iiFTcmtzQI0yEYy9bq6IOw7lxiuVQenu75V0nQA5WM6aZB9UguRKbg+f+YLTo1uXW5gF+oNllaSLtkwlIETMhuivDKu0NGX1LVV6Wm/7A5QW7avMC1LtgLRzYevo/mQwhR82ndLauhnzl2r/iQZ0HaKQP9AjWKz/5gAQAA'
}

# File: /etc/webmin/virtual-server/backups/200000000000000
file_ewvb2() {
    printf 'H4sIAKC/9VgAA11Qu1LEMAzs9R/uYByuolEPBRUVlceJN4cnfmRk5+D+HuVyFKAZaWRJu14JIlUanyigdbajn5Ztta9lEmSU7pM1Hyab4MyLeTPvdInSN59yLExzTGCLPtkvjFqx9+Zjg1wgd7JmT8MfoyWmxAN5jU+04Mrk5w5hOgCurr25EIXVS0313Hh4uOWfNUMfVAvGqwbFI/uYmGLg/9+syavIdZMz+Jlal7nHvENGzFXAhOLHhKB8NxIXam66Ve56jtWLrq/DM3zfBHvjyNyh+xg+gBAVn5ddsgr6ntIWlD7+HlGLP1VK7OhnAQAA'
}

# File: /etc/cron.hourly/firewall-cmd-blacklist
file_ecf() {
    printf 'H4sIAO+A7FgAA03MTQrCMBAG0H1O8YlFdDEMuhV7BUW8QH6mGJxMShrr9QVXrh+87YZDNg5+ebqpNtyu9weyYdhH38HSI0+5ycerUiyJgvr40rz0wxmpOgD/DKImpa5Cc239Mvw2olla8SbWXaomGMFJVra3Kk7j7ui+iQC34oUAAAA='
}

# File: /etc/httpd/conf.d/performance.conf
file_ehcp() {
    printf 'H4sIAMIOAFkAA31T20rkQBB9z1cUzAfsCiK+LrO4CI47OIKPS6W7MlPYl9iXqH9vVSbJOKyYl0B16tz6ZNWs4FeP5kCwjomaVaMDH2soEDso7AmKHGZKAyV4ZefgFblAFxMYSgU5AA0USoaWZEjQITsOe0BI9FIpl+ZRUP7WAufPxfVPITvxb7Yb0eB9DPCbE5nCA+WjoPvqW2EXQebAzs5q+hQN5UwZTCIsZAEL5IKp1L7Z6Xs3fpg/014q4IbDjw2+QViA2Tp1Kjg2Aw7iAVudRDhg0LPJDOSen0WXIOx6TPQ4rczPdSO4X51cXI3E+Ma++k/EJoagZmPIwi8GxoxbWtyJXVlxBQPFmt27EjwcxTzF9LzYu7o8j2o2M2fTvgNJ0FOEE3ozqdxSWo/zkw8Bu2MvVy0XohX4RjIG4GB5YFvRyd7ZLY1+phBtTVoNlrY47kitrE9giwi1claNaKts+2j/dWbP9tiK/7O8wVzWf24B+96xQQU9laS50VVZ2i61ma0Klp/AvoEYJesfcUSaYO5kqAXXPkuhPwA8dxsXUQMAAA=='
}

# File: /etc/nginx/conf.d/performance.conf
file_encp() {
    printf 'H4sIANsRAFkAA82RMW+DQAyFd36FpawRbYeqlTq2apWhU7sjcphwEpzR2QnJv6+PgzTQJFOGsoDOx3vP71skC/ikYlsjuM0+q0TazJDHrOkPk0WiF75QGHIQ2yBtBUryID533FgR6zY68sgtOUYQAqkQTG3RSQrf+j3+ZhkYReXI1QdYo3SIDqQj4K0xyGx3CJ23gkAt+lysKi7B0eAYpKKrXiUHVIYz1esq0vhjhBRW5UkGKAi5F/FoMFjk7iBViN3Z8Na7miyEXKpW/yM5hybYh8ymJsYiTRhdkY27/D4Pz/cviZZ0psfW0/4wKfINS+tw3iWy5OvachW7PLEPCfUkCFkstD6/Q58mUXi4d5Lp6ZFfLtt4zAt1uLuErvTU9PsPdio0GJ7BeBVicJozVLUrFC8xnG5+ZBlADVtcwXkFZmww5JwgVZjaYBz+4R2H51GXOYvZ2FvAflep14/VEfYoPcd9I9iD3X+APd38drDHBme4I89xOMMdhz+XIolDHgUAAA=='
}


# File: /etc/httpd/conf.d/000-default.conf
file_ehc0() {
    printf 'H4sIAPlj+FgAA4WPzQrCQAyE73mKQM91rfYgIl78QVBEqHiVZU3bpbEr21jo21u1oChiLkNm5oMkgADnlOorCx6sl6tmXLlKIICEfE1+q8+Ex9OzcgSYdK176eWPR/0pIGKAWye5LTPMyRNM1Ft7+ouN4+EDTpLNosxsSejKbt95J844Rs2MYWvUg6cMMdxvkjrqpBd1wIy82NQaLbS0TKhIjLoUVglXyrRhpdgZzXl7Qs94+cbW1HyTF2/rNntjC2o+3sO/A3ADtXiMKm8BAAA='
}

# File: /var/www/html/index.html
file_vwhi() {
    printf 'H4sIAGRw5FgAA+1ba28b1xH97l/BMggQA7ur+36oUoBGdtICCRLASYp8CuQ1bQmhRUGiZTtF/3vPOZekqFSyWQexG8ACRO7ufc2dO48zM8uDvzz49uj7n757ODlZPp9/fu+gfeF7dvzk83sT/B1cLl/PZ+2af5dXzyb/2tzx7+Xpk+XJ/sTGT/964/nJ7PTZyXJ/cvxiubjZ8vz44tnpWT+fPX1T88Vdw5+cXp7Pj1/vTx7PF+MvN9vOF5eny9PF2f7kYjY/Xp5ezW62Lxfn+5NofkPq8uL47PLp4uL5frvEyNlPn3nz6f3rfv9u7Njb4sfB8nSJ6wezp8cv5svJP2ePH80urmYXk++On80O9lrrvYO9xs17B48XT16vuQo2ouclSD2c2sFOJ6dPDqdfzo6XJ7OL6eTV8/nZ5eH0ZLk839/be/ny5fDSD4uLZ3vOGLOHwasu+6/mp2e/3NbR1lr31Iquh1Nz/mo6eb36vjqdvfxiwacTM3G+uiFOgnNpSNPJ7Oz48XzWPz4ef3l2sXhxBqrOZi8nt/QEAfuX58fj7HB6fjG75M6n13JygKVnxxdfXRw/OZ2dLbW9Rz9+9Y8HP9ufp5Nnq8c/nJ0usc8XGP2IU3179sPlDFPbw2kfbcqDqcmBcNynmocYQ0WrQ2uI2QzWqhX31tc4JFuvp/5+faaH0+fHy4vTV5+ZgaRPejNUk/xk86Wn3oYwFGvKpA+YZ3C1+Ptb+1npwuJ8snj69HK2BPOmE8nC4ZSP+3ExX1zsf/JlqtX56WTvjUMHb9nptvG5/m2H8cWX28c/rLm4dGP8wd7Ns9hqOYe8TZ6ezuc4g4v5Z5+sT+j+dILz+sbmWobaOXD6yGbvhtRFsKYD2yEJnc2ms6HaznszxLlFv9I5V8YCVvbW2cGzz1B7hw6hczHjuXd+SGMacp/xzJrB9fio11dj79GATn3CfHnwfOq61uTKgPVsby07WS7cu+D4kE0j+Ir1EvrjRDHUWxAaUsVske0BTb3DZYEIsT1zgevLR7b6iDmTwWa39v/rTZZuMe5scTZbsSvWIKIwII/ct+jGUjjwzmHHZFNvtW9uBorfaQOdxZ4tHoYjGwv5jEn0FUshpz2YybutBXaiKGSuYbOr2HtfuRnu33Tkkid5kWuG2Lp5HBjEn1Rnr6/tCXZZMXluxCVw1I2WXOXBr87SUyyw6EgL0mct70CI9QW9MuTEBPAFa/k+B0pboUwEji346J3F5wi6bZ8xGaURM2It2xUQiPbAVoiWA8cTxCSA0V3lI2cK5w6YMQYKBWnBiXfYYezB6CGMlE+bwWeQh42AWyC/j5CFEc9Cr81pgYQuHRbOPeySHy35Sbo7l/EMgl47LB36kkYsBs6HjgTaorPF9OhXR85EASUFXkcTJCHQHzxLuKL0gAjXhAW76rkzTukxjtzApsFHqkCPD9dB+cBtHCRWiBjmOkd1dKELFctBumsH5maoGVhEVmMvFDFoNjeD4zJQFzS7xhyL2aifgccIAQRXQCAoovJ1OEVPbaHkcDs4h4QOTVlpLGinyZDMDn0h37hAULPtcFKRzRDrQIWl7cB4KOQoNbYJW6A9cGQejw0DITdSW9kAS7FKgRRQz+fisYeBmVuKGTgIPvMbFGCLGoMR8SgWQ55miBZYaTACN5Yb3Jbi3fQMA6Q0kIivb9yNXMN0ZIKuQIHMKfuE1gcSwTt/4249/u7lP/nioTPu4YoCXzWm+kCZoLRCWgrtn+9o0ELGzIVcFRkyTY0oz+9RmiZ+S8YCpYbaW44sDWPi1BBKXzgzrlOTmetFbyN0Aad+unwtjxenfx66byOUh5LaocTRiBK7/qRxvXG7JnOkzhhthc/oziiJ8FNUG6o+TDnsCnrQGvcra1yo/2sLDPMn0ZC8bFPx9Rvu3vE0/tSbvBNyuh0hZ42QEh9sWUFOkB1NKCvIGT3cFkxXQ5zXje8PcQ7ATrfDvopDTOGtsDF5424df1Sd8fmt43O04fbxD0BZfMt4+4chVrdBrC5G2nlLszHSCfSJ7iB7wYzWRm/rjB09gGKmuBYCgCZXcB6htzWPTlYkw9USqMLP0Rd7jEF7coIrtDzOyQXRjQItDgUumzC4Jwzu6d+sbW4fbhhd6fZjR2gB90lPRxfWZ0uIgm6G6JaeGO4X8AEAJ2IuuUQrAGKd4GAmeOiDdI6YpUiJsIoXRZijmM0S0dNDV85hZC0N4Y+FxyQ8EDIFciKckXeE9xv83GJeYmgI+REmJk4CoJA9dQ3YOyCLG+zeVRv9rtpoAD6cyU0ZQTXWKHWtjSnSR0N0VgHgdfMHjgAdpJlh6RtVoQ4xFF/NrL9DITHFw/Lg7QpZyu3j65c7GYQaSr5LoY13v08r/VorIyQa2KoAXcKHKEykeFHE80qwoVK4zIT/K9fO6AnSpqiJYQsRaUftJIBlHNArNovCxgEyH6hCMNFQzbQG+YT1fpRzcQTQhag5Jy1b+AnKCgNOIXpPSMvIlXajUC1IEcIVaJ1nAArk7QRJAgIXdiIS9qOQPXEjQ7sGi4FCKjuCOIacjGTgEw2XJw0YaEloJg6+hjRxhQ8zvv24wooKEVsf8gLBhsJXBWbON9tV5VsLgxBEVGG0Ik4xFwOsmMgfCF2PcDIdWWPAuRKswgXaHFcKzU3KNJauREs7lmWMhnoUA62WK5noNAavPoVM3T7ZXfU/7Kr/HpyzdA3Uf4L1YvPaFxssX2uwTfs3jR998Qf3xWGj9RVClhi8jfR69HJyZwwkS6ccUYvzHF1YYogMOQ2SZsglY/AWKDKMXGULmqZRURhn9i3O9JbKkDraELQydGOY2rcwFQCzcjSC3cDZ5TUZ5/YtzrVWWtMpcRSYImGI3CtElisHuQrmS68YIvm+hdZWbtMrKKetoVonIQAZN/nOceNYlaOgr6fVadYstXxaYS7D0qEKAMCK0EKo3WG1QbvLiudBvkmKYzSDEg4xtnSD5XokEV1T4gSKJZmEsDIVROmMiK/hh2dwHmn5HCMkhveMi5iFgZ32XDWL9qqolPC+IMAC+QjHrGyKMAMNFvWxD0EbETqxFoxkciK2FIC3SveRJbKCrgXwWUgktBSCo638Olru3BVveACwXpFAif2tde0ccclYHyE8N4RAnQcQKwXLJcds0rX07WqX4s64hLGHcXUVJTB5FcwGl2TIFtDlCpdct36EJf8nsCSuDZSXW4TDrMIQRMiV6qjMn61VCKUDw5uL9nLDMDOCJkTTdN5ByUoEF4XZTsspeY0rGIuxCoT4ppKQ6cwgN9GemKaiwjhGOlqpQDJ6hsppxy2wIgxRpea5DWESvVejku1RxoRm1nn5bYYcsG3MRoYinYWNpauWRsFcHrlmDKJvExLi+JIYqZiuFXlG5qH6qhiC6UBL1bPEagAhyhwK9BAPgQ+0Ap5oAkYqkjfMjlqhNeU3bQNI0PoC25lWNtP3YIaCM1p6JugigVngylapvERzzOAqY0bHjrWxjH6j5WpDS/Kts0JO58Tkg1nbY2aU0OnIKxOJM7cy9ERIuGECdFsadjUZ6Z2gjDXM5jtbbgczW80f4cwHhzNpk1pAsClATCwxSnqVMe9sg+D0Q83xNXxMdJAbpskU0AAXqQQn61dMxK8giWX1KXcqI/SS6NT0RnkC6jMnMfL3NAgs1TCy8DICleCgxTkto54B4uFMQSeT31LKVvloaCExRohVyXrHZL1lNl91tQQ/TSgRler3fZFy2FZjoG4QVfRRCQ+vtEjtWsFGWQQmMpzMY2H6VfW8scVjiYWHyAqBlbHUvCpBrEM8K/saeuU85go9pOjET7S6sV/lFvuWW2QQpIIAlZiQ0ak0IPOZAQPHxO2lIAQkAxlkshXkOQALz3mSgkVVoAI4RpPcM7sRCTmZlhlUaMCCgdaxQVZvV+hJJx8IFGsnkAkTTpyoY2StIfEUYWFZcfOCgThDFnZCA0+Eq8xWsy42N53h/6ZcsA79aMPsSM7lFtKSALoXcbHS3SAWUzmHMuEYTMtR0Iz29BZxVe65o/rXIKpRYM0FxWIQaf+LpBsVDGaHuxst6huqVsAMKropVmzxNCgpo4pjzWo7FbdaVsyqdGtBYmSczgwcxd8qmh4ZFKQVbG7aRb+gPMLIyJ/cLFQ+347F18Yr+C86vY5e0bqWi5PQhJHiS9lXDq+sKnKUdCsAQN6yFNMj4kXM37JmdNhJPr7havQzVNaoOtRICYg6zMwiZ+yUr2MendVFVf9aGkHAF5xSrZGnQ2NgVSD0NBRxw/g+XBdNfcuqq3whGVMU5UikFV4ORRNGYV8Wv5odcfKAdsU/QZmqMyTzjWomzDYoXIBE81BqJ2nOkpeqOEcpjR6Wi9wewhwsojUJqrPQN7OqmVTFo5msvFN+f9to7upV865eNZEvPjSvWkFRpg9bJesjk4fMCdCnbho/etQP7lHzxqMmR1MTGeOOLExDZ1jZzqrlWuUCLJU9tUDa6aUQvgGRZRGkhRwJp6Oif2hZagWKMpVGrlpZcITt2eqFkaQwm1Vor1anVqJZKYleKbGmla1YhJeoU8dhHVRljF75c2XbjVU9vOkePYP1zXzBFDNQ8DRkYVUjh90BegYQkB3yig1slsGUtaJVSvQdclNWdBdNXZknYYqxZ8KvTa3tehlSw0Iz9+3keUBiaLlIaKY9slVvQUSvd0ggFmR0ZExyzf+dy8yptpqqKvhvcGZH1jc+Vb2dg4FavTY/vD3NuxaO/3BK7jRQ5d1gPyDLkF2yd8D+6+aPRuqDG6myMVLvSeLvqsZv3iO5o8jON0lKq523YNzHVlePeiXnzvdSfscLAu+JpDvVr76j+gEgZxfuKCFsNX9Uvw+ufnWjfu9bB+4S+rh6O+v2uGf9f3Tj/ZRue+TNu99Wy/8X/fuDSbn7zW2zo+KlCkMYTHYbzQOysRvHlwyDjFrjRvPWzR8174NrHg75/nsW+5WcpwfpYbZtbedUnvclX0FW8gkzaVeslp8giM1XvS6JXn9El7+33r9OvnE+a9C8V1I7IWaYt5o+L08YjvsrJr1OOOsVcxJ53hJxiKlPEKrqrufdFRvVr62Bqa+JP+CPMPjbjvabDv7Igz+d+Q8r1TuuUTMAAA=='
}

#File: /var/www/nginx/index.html
file_vwni() {
    printf 'H4sIAJ915FgAA71VS4/bNhC+769gGRTIHkjxLdKRckjTcwO0QJGjvKYtZWnJkLT2bov+9wwpv3e3C+QQHcwZzoMzH78xi18+//HbX1+//I7qcR0+3hTTAquvFh9vEHzFMD4FP8nxG7Yr9O9Ri9+uWYz1DGn264eL/do3q3qcoeph7C4t66pfNS0Jfvl/5v618EUzbEL1NEPz0N3dX9o23dCMTdfOUO9DNTZbf2kfu80LpY591Q7Lrl/PJhEi/df34HZ78vtvgiM7w6MYmxHkz35ZPYQR/e3nf/p+63v0pVr5IpusN0U2oXlTzLvF0wFVgPFxHdqhxPU4bmZZttvt6E7Srl9lgjGWgQeeXGaPoWnvX3LkzrksWTHaNn73qXssMUMMca4p5zbXSCgKq8g1Pt1hsTp1XOJjx+8J51QbITSKEhdKiNuzsBS6qcYaNYsSVxjB75orylRupUVS0HiOE1vCHWVGGadDkoRyDh33zswYLZsQStx2rcdoGPvu3pf4HXPsoBFozt9VmxL33UO7uNj+1jXt9X4iY4khc/ZS4bFipaiJyMQWYSV7jQrLhSaaSliQowaQODNG1wsFHT2lkmDL+VmamlM1OTM6bcdsISUgYANMNJLUJd2COU+HSwr7NQFIY+pAIJrHwEm/g0gpWG4FoYxbY4mgWmrDYn6eO2bNaQeEIVaqrJBRAf/coXNzMDSeOeWG1LGgJNckj0UHKMfFQolJPYAGfYItulgq81wIHjh1OuEARbPYRa0P1UsoKiGcU8NiH+Si/5pMqESuKWZVzv858GFiwKs3mDPoXwIO6Q5jjgQNs84Cs7iRDnBQVkPSeCHwGR530o1ECKC8rQFMlGauBkIe5LsYrV3C2VnBQRCMKxHzaWC5kmQffljfKPlh8Pv/wRJzpgWVMF74fPrW1dg3jzB6KM0tdAQtSQdXBHNlFTdC3mK0p3UO3GVOKQv/DHHqZ3Xvl3B29fzkV0acQhUWAOOISsk4E8Jej/jlmM/3Y84UkMFJoJqEq1eCTRNtleVJ4LnV5ifO8w/CC/1SJ7SViL0N6/w5rNnqbVJKDawBirsTK8kVLck1LQ982saZV0ZJI4GYR3lPTPSMmOiamOhNYh57KOIbE9+n6V2KD1V8/r8DmVpGwxUIAAA='
}

# File: /etc/php-fpm.d/default.conf
file_epd() {
    printf 'H4sIAOkf5FgAA02MQQqAIBBF956iCyTWJgo6SUSITimMNuhIHT+hFvE3/z0+f7Gw64K8ipIhNXNDjtqdgjjSWejH6DNDrKLrB6lqumlUSn1easTzArsZ9BA5/3eCQsXMmr2pXQZ9b8Z5tOn9Ew+XgZOUhAAAAA=='
}

#############
# Main trap #
  time main #
#############